package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String, Double> mapTest = new HashMap<>();
    double getBookPrice(String isbn) {
        mapTest.put("1", 10.0);
        mapTest.put("2", 20.0);
        mapTest.put("3", 30.0);
        mapTest.put("4", 40.0);
        mapTest.put("5", 50.0);
        if (mapTest.containsKey(isbn)) {
            return mapTest.get(isbn);
        }
        return 0;
    }
}
